'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Workspace extends Model {
    static associate(models) {
      Workspace.belongsTo(models.App, {
        foreignKey: "app_id",
        as: "app",
        allowNull: false
      });
      Workspace.hasMany(models.Conversation, {
        foreignKey: "workspace_id",
        as: "conversations",
      });
      Workspace.hasMany(models.Bot, {
        foreignKey: "workspace_id",
        as: "bots",
      });
      Workspace.hasMany(models.User, {
        foreignKey: "workspace_id",
        as: "users",
      });
    }
  };
  Workspace.init({
    slackId: {
      type: DataTypes.STRING,
      unique: true,
    },
    name: DataTypes.STRING,
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'Workspace',
  });
  return Workspace;
};
