'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bot extends Model {
    static associate(models) {
      Bot.belongsTo(models.Workspace, {
        foreignKey: "workspace_id",
        as: "workspace",
        allowNull: false
      });
    }
  };
  Bot.init({
    slackId: {
      type: DataTypes.STRING,
      unique: true,
    },
    userId: DataTypes.STRING,
    scopes: DataTypes.STRING,
    token: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Bot',
  });
  return Bot;
};
