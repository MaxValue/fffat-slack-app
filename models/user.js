'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.belongsTo(models.Workspace, {
        foreignKey: "workspace_id",
        as: "workspace",
        allowNull: false
      });
      User.belongsToMany(models.Conversation, {
        through: "ConversationMembers",
        as: "conversations",
        foreignKey: "user_id"
      });
      User.hasMany(models.Invitelink, {
        as: "invitelinks",
        foreignKey: "user_id"
      });
    }
  };
  User.init({
    slackId: {
      type: DataTypes.STRING,
      unique: true,
    },
    // type: DataTypes.ENUM,
    scopes: DataTypes.STRING,
    token: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
