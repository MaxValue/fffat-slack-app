'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class App extends Model {
    static associate(models) {
      App.hasMany(models.Workspace, {
        foreignKey: "app_id",
        as: "workspaces"
      })
    }
  };
  App.init({
    slackId: {
      type: DataTypes.STRING,
      unique: true,
    }
  }, {
    sequelize,
    modelName: 'App',
  });
  return App;
};
