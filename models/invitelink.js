'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Invitelink extends Model {
    static associate(models) {
      Invitelink.belongsTo(models.Conversation, {
        foreignKey: "conversation_id",
        as: "conversation",
        allowNull: false
      });
      Invitelink.belongsTo(models.User, {
        as: "owner",
        foreignKey: "user_id",
        allowNull: false
      });
    }
  };
  Invitelink.init({
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    }
  }, {
    sequelize,
    modelName: 'Invitelink',
  });
  return Invitelink;
};
