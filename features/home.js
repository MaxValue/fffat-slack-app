var models  = require("../models");
const { inferConversationType, isError } = require("../util");

module.exports = {
  init: init,
  render: render,
}

async function init(app) {

  // when app home is opened
  app.event('app_home_opened', async ({ client, body }) => {
    await render(client, body.api_app_id, body.team_id, body.event.user);
  });
}

async function render(client, app_id, team_id, user_id) {
  models.App.findOne({
    where: {
      slackId: app_id
    },
    include: [{
      association: "workspaces",
      where: {
        slackId: team_id
      },
      include: [{
        association: "users",
        where: {
          slackId: user_id
        },
        include: [{
          association: "invitelinks",
          include: [{
            association: "conversation"
          }],
          order: [['id', 'ASC']]
        }]
      }]
    }]
  })
  .then(async (dbApp) => {
    let view = await render_view(dbApp.workspaces[0].users[0].invitelinks);
    const result = await client.views.publish({
      user_id: user_id,
      view: view,
    });
  })
  .catch(async (error) => {
    console.log(error);
  })
}

async function render_view(invitelinks) {
  let blocks = [
    {
      "type": "header",
      "text": {
        "type": "plain_text",
        "text": "Einladungslinks",
        "emoji": true
      }
    }
  ];
  let currentOptions = [];
  let currentOptions_checked = [];
  let invitelinksIDStart = null;
  let invitelinksIDEnd = null;
  let invitelinksEnumerationStart = 1;
  let invitelinksEnumerationEnd = 0;
  invitelinks.forEach(function(invitelink){
    if (!invitelinksIDStart || invitelink.id<invitelinksIDStart) {
      invitelinksIDStart = invitelink.id;
    }
    if (!invitelinksIDEnd || invitelink.id>invitelinksIDEnd) {
      invitelinksIDEnd = invitelink.id;
    }
    invitelinksEnumerationEnd++;
    let newOption = {
      "text": {
        "type": "mrkdwn",
        "text": `#${invitelink.conversation.name}`,
      },
      "description": {
        "type": "mrkdwn",
        "text": (invitelink.active ? "Aktiv" : "Inaktiv"),
      },
      "value": `${invitelink.id}`,
    }
    currentOptions.push(newOption);
    if (invitelink.active) {
      currentOptions_checked.push(newOption);
    }
    if (currentOptions.length==10) {
      blocks.push(render_invitelinksBlock(invitelinksIDStart, invitelinksIDEnd, invitelinksEnumerationStart, invitelinksEnumerationEnd, currentOptions, currentOptions_checked));
      invitelinksEnumerationStart += 10;
      currentOptions = [];
      currentOptions_checked = [];
    }
  })
  if (currentOptions.length>0) {
    blocks.push(render_invitelinksBlock(invitelinksIDStart, invitelinksIDEnd, invitelinksEnumerationStart, invitelinksEnumerationEnd, currentOptions, currentOptions_checked));
  }
  blocks.push(
    {
      "type": "divider",
    }
  );
  return {
    "type": "home",
    "blocks": blocks,
  }
}

function render_invitelinksBlock(idStart, idEnd, enumerationStart, enumerationEnd, options, checked) {
  return {
    "type": "section",
    "block_id": `${idStart} ${idEnd}`,
    "text": {
      "type": "mrkdwn",
      "text": `Einladungslinks ${enumerationStart} bis ${enumerationEnd}`
    },
    "accessory": {
      "type": "checkboxes",
      "action_id": "toggle_invitelink",
      "options": options,
      "initial_options": checked,
      "confirm": {
        "title": {
            "type": "plain_text",
            "text": "Bestätigung",
        },
        "text": {
            "type": "mrkdwn",
            "text": "Bist du dir sicher, dass du diesen Einladungslink löschen möchtest?",
        },
        "confirm": {
            "type": "plain_text",
            "text": "Ja - Löschen",
        },
        "deny": {
            "type": "plain_text",
            "text": "Halt Stop",
        }
      }
    }
  }
}
