"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.createTable("Conversations", {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          workspace_id: {
            type: Sequelize.INTEGER,
            references: {
              model: "Workspaces",
              key: "id"
            },
            allowNull: false
          },
          slackId: {
            type: Sequelize.STRING
          },
          name: {
            type: Sequelize.STRING
          },
          type: {
            type: Sequelize.ENUM("open","closed","mpim","im")
          },
          archived: {
            type: Sequelize.BOOLEAN
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
          }
        }),
        queryInterface.createTable("")
      ]);
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Conversations");
  }
};
