'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.createTable('Invitelinks', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          conversation_id: {
            type: Sequelize.INTEGER,
            references: {
              model: "Conversations",
              key: "id"
            },
            allowNull: false
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: "Users",
              key: "id"
            },
            allowNull: false
          },
          active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
          }
        }),
      ]);
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Invitelinks');
  }
};
