---
ansible_host: IPADDRESS
ansible_port: SSHPORT
ansible_user: USERWITHROOTPRIVILEGES
CJB_OWNER: service_slack_cjb
CJB_GROUP: service_slack_cjb
CJB_OWNER_HOME: /var/projects/slack_cjb
CJB_INSTALL_DIR: /var/projects/slack_cjb/server
CJB_SHELL: /bin/bash
CJB_DB_NAME_DEV: service_slack_cjb_development
CJB_DB_NAME_TEST: service_slack_cjb_testing
CJB_DB_NAME: service_slack_cjb
CJB_DB_USER: service_slack_cjb
CJB_DB_PASSWORD: AHIGHLIGHYSECUREDATABASEPASSWORD
